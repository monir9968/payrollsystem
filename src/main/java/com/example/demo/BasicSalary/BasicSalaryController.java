package com.example.demo.BasicSalary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BasicSalaryController {
    
    @Autowired
    private BasicSalaryService basicSalaryService;
    
    @GetMapping(value = "/add/basic/salary")
    public String addBasicSalary(Model model){
        model.addAttribute("basicSalary",basicSalaryService.get());
        return "basicSalary/addBasicSalary";
    }
    
    @PostMapping(value = "/basic/salary/save")
    @ResponseBody
    public boolean save(@RequestParam(value = "basicSalary") double basicAmount){
        return basicSalaryService.save(basicAmount);
    }
}
