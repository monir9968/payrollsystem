package com.example.demo.BasicSalary;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class BasicSalaryDao {
    
    @Autowired
    private EntityManager entityManager;
    
    public boolean save(double amount){
        Session session = entityManager.unwrap(Session.class);
        if(getBasicSalary() == null){
            try {
                String sql = "insert into basic_salary(id,amount) values (1 ,"+amount+")";
                SQLQuery query = session.createSQLQuery(sql);
                query.executeUpdate();
                return true;
            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }else {
            try {
                String sql = "update basic_salary set amount="+amount+" where id=1";
                SQLQuery query = session.createSQLQuery(sql);
                query.executeUpdate();
                return true;
            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }
    }
    
    public BasicSalary getBasicSalary(){
        Session session = entityManager.unwrap(Session.class);
        try {
            BasicSalary basicSalary = session.get(BasicSalary.class,1);
            return basicSalary;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
