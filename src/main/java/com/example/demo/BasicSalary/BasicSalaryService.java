package com.example.demo.BasicSalary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasicSalaryService {
    
    @Autowired
    private BasicSalaryDao basicSalaryDao;
    
    public boolean save(double amount){
        return basicSalaryDao.save(amount);
    }
    
    
    public BasicSalary get(){
        return basicSalaryDao.getBasicSalary();
    }
}
