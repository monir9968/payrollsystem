package com.example.demo.CompanyBankAccount;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "company_bank_account")
public class CompanyBankAccount {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String companyBankAccName;
    private double amount;
}
