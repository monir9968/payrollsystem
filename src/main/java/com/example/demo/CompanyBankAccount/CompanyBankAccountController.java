package com.example.demo.CompanyBankAccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("company/bank/account")
public class CompanyBankAccountController {
    
    @Autowired
    private CompanyBankAccountService companyBankAccountService;
    
    @GetMapping(value = "/create")
    public String create(Model model){
        model.addAttribute("companyBankAccount",companyBankAccountService.get());
        return "companyBankAccount/companyBankAcc";
    }
    
    @PostMapping(value = "/add/balance")
    @ResponseBody
    public boolean save(@RequestParam(value = "amountCompanyBankAcc") double amount){
        return companyBankAccountService.save(amount);
    }
}
