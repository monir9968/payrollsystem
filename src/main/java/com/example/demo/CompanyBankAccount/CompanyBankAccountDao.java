package com.example.demo.CompanyBankAccount;


import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class CompanyBankAccountDao {
    
    @Autowired
    private EntityManager entityManager;
    
    public boolean save(double amount){
        Session session = entityManager.unwrap(Session.class);
        if(getCompanyAccAmount() == null){
            try {
                String sql = "insert into company_bank_account(id,company_bank_acc_name,amount) values (1,'ibcs-primax',"+amount+")";
                SQLQuery query = session.createSQLQuery(sql);
                query.executeUpdate();
                return true;
            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }else {
            try {
                String sql = "update company_bank_account set amount="+amount+" where id=1";
                SQLQuery query = session.createSQLQuery(sql);
                query.executeUpdate();
                return true;
            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }
    }
    
    public CompanyBankAccount getCompanyAccAmount(){
        Session session = entityManager.unwrap(Session.class);
        try {
            CompanyBankAccount companyBankAccount = session.get(CompanyBankAccount.class,1);
            return companyBankAccount;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
