package com.example.demo.CompanyBankAccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyBankAccountService {
    
    @Autowired
    private CompanyBankAccountDao companyBankAccountDao;
    
    public boolean save(double amount){
        return companyBankAccountDao.save(amount);
    }
    
    public CompanyBankAccount get(){
        return companyBankAccountDao.getCompanyAccAmount();
    }
}
