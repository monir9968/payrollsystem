package com.example.demo.bankAccount;

import com.example.demo.employee.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BankAccount {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "account_type")
    private String accountType;
    @Column(name = "account_name")
    private String accountName;
    @Column(unique = true,nullable = false)
    private String accountNumber;
    @Column(name = "current_balance")
    private double currentBalance;
    @Column(name = "bank_name")
    private String bankName;
    @Column(name = "branch_name")
    private String branchName;
    @OneToOne(cascade = CascadeType.MERGE)
    private Employee employee;
}
