package com.example.demo.bankAccount;

import com.example.demo.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/bank/account")
public class BankAccountController {
    
    @Autowired
    private BankAccountService bankAccountService;
    
    @Autowired
    private EmployeeService employeeService;

    @GetMapping(value = "/create")
    public String create(Model model, @RequestParam(value = "empId") int empId){
        model.addAttribute("employee",employeeService.get(empId));
        return "account/create";
    }
    
    @PostMapping(value = "/save")
    public String save(BankAccount bankAccount){
        bankAccountService.save(bankAccount);
        return "redirect:/employee/list";
    }
    
    @GetMapping(value = "/update")
    public String update(Model model,@RequestParam(value = "accId") int id){
        model.addAttribute("bankAccount",bankAccountService.get(id));
        System.out.println("bank acc update : "+bankAccountService.get(id));
        return "account/create";
    }
}
