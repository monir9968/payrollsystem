package com.example.demo.bankAccount;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class BankAccountDao {
    
    @Autowired
    private EntityManager entityManager;
    
    public BankAccount save(BankAccount bankAccount){
        Session session = entityManager.unwrap(Session.class);
        try {
            session.saveOrUpdate(bankAccount);
            session.flush();
            return bankAccount;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public BankAccount get(int id){
        Session session = entityManager.unwrap(Session.class);
        try {
            BankAccount bankAccount = session.get(BankAccount.class,id);
            return bankAccount;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
