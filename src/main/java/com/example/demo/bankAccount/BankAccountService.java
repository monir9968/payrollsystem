package com.example.demo.bankAccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankAccountService {
    
    @Autowired
    private BankAccountDao bankAccountDao;
    
    public BankAccount save(BankAccount bankAccount){
        return bankAccountDao.save(bankAccount);
    }
    
    public BankAccount get(int id){
        return bankAccountDao.get(id);
    }
}
