package com.example.demo.employee;

import com.example.demo.bankAccount.BankAccount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Employee {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "employee_id")
    private String empID;
    @Column(name = "employee_name")
    private String empName;
    @Column(name = "rank")
    private String rank;
    @Column(name = "address")
    private String address;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "basic_salary",nullable = false)
    private double basicSalary=0;
    @Column(name = "gross_salary",nullable = false)
    private double grossSalary=0;
    @OneToOne(cascade = CascadeType.REFRESH,fetch = FetchType.LAZY)
    private BankAccount bankAccount;
}
