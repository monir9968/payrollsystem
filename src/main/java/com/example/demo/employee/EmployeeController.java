package com.example.demo.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
    
    @Autowired
    private EmployeeService employeeService;
    
    @GetMapping(value = "/create")
    public String create(){
        return "employee/create";
    }
    
    @PostMapping(value = "/save")
    public String save(Employee employee){
        employeeService.save(employee);
        return "redirect:/employee/list";
    }
    
    @GetMapping(value = "/list")
    public String list(Model model){
        model.addAttribute("employees",employeeService.list());
        return "employee/list";
    }
    
    @GetMapping(value = "/update")
    public String update(Model model, @RequestParam(value = "empId") int empId){
        model.addAttribute("employee",employeeService.get(empId));
        return "employee/create";
    }
    
    @GetMapping(value = "/get/employee")
    @ResponseBody
    public int getEmployeeId(@RequestParam(value = "empId") String empId){
        return employeeService.getEmployeeId(empId);
    }
}
