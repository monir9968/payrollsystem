package com.example.demo.employee;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class EmployeeDao {
    
    @Autowired
    private EntityManager entityManager;
    
    public Employee save(Employee employee){
        Session session = entityManager.unwrap(Session.class);
        try {
            session.saveOrUpdate(employee);
            String temp=employee.getId().toString();
            int empIdLength=temp.length();
            for(int i=0;i<4-empIdLength;i++)
            {
                temp="0"+temp;
            }
            employee.setEmpID(temp);
            session.flush();
            return employee;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public List<Employee> list(){
        Session session = entityManager.unwrap(Session.class);
        List<Employee> employees=new ArrayList<>();
        try {
            Criteria criteria = session.createCriteria(Employee.class);
            employees=criteria.list();
        }catch (Exception e){
            e.printStackTrace();
        }
        return employees;
    }
    
    public Employee get(int id){
        Session session = entityManager.unwrap(Session.class);
        try {
            Employee employee=session.get(Employee.class,id);
            return employee;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public int getEmployeeId(String empID){
        Session session = entityManager.unwrap(Session.class);
        try {
            String sql = "SELECT count(employee_id) from employee WHERE employee_id="+empID;
            SQLQuery query = session.createSQLQuery(sql);
            return Integer.parseInt(query.uniqueResult().toString());
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }
}
