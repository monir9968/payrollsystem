package com.example.demo.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    
    @Autowired
    private EmployeeDao employeeDao;
    
    public Employee save(Employee employee){
        return employeeDao.save(employee);
    }
    
    public List<Employee> list(){
        return employeeDao.list();
    }
    
    public Employee get(int id){
        return employeeDao.get(id);
    }
    
    public int getEmployeeId(String empId){
        return employeeDao.getEmployeeId(empId);
    }
}
