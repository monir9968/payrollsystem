function addBasicSalary() {
    var basicSalary = $("#basicSalary").val();

    if(basicSalary.length>0){
        basicSalary=parseFloat(basicSalary).toFixed(2);
        $.ajax({
            type:"post",
            url:"/basic/salary/save?basicSalary="+basicSalary,
            success:function (data) {
                if(data){
                    alert("Successfully inserted Basic salary !")
                }
            }
        });
    }
}

function addAmountOfCompanyBankAccount() {
    var balanceOfCompanyBankAcc = $("#companyBankAccountAmount").val();

    if(balanceOfCompanyBankAcc.length>0){
        balanceOfCompanyBankAcc=parseFloat(balanceOfCompanyBankAcc).toFixed(2);
        $.ajax({
            type:"post",
            url:"/company/bank/account/add/balance?amountCompanyBankAcc="+balanceOfCompanyBankAcc,
            success:function (data) {
                if(data){
                    alert("Successfully inserted Basic salary !")
                }
            }
        });
    }
}