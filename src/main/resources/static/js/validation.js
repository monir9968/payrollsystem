// function checkUnique() {
//     var empID=$("#empID").val();
//     console.log("emp : "+empID);
//     if(empID != null && empID.length !=0 && empID != ""){
//         $.ajax({
//             type:"get",
//             url:"/employee/get/employee?empId="+empID,
//             success:function (data) {
//                 console.log("data ; "+data)
//             }
//         });
//     }
// }


function numberValidation(event) {
    var regex = /^[0-9]+$/
    var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}

function nameValidation(event) {
    //var regex = /^[A-Za-z. ]+$/;
    var regex = /^[A-Za-z\'\s\.\s\-]+$/
    var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}

function mobileNoValidation(div_id) {
    var no = $('#'+div_id).val();
    if(no.length==0) {
        return;
    }

    if(no[0]!=0 || no[0] != "0") {
        $('#'+div_id).val("");
        return;
    }

    if(no.length<2) {
        return;
    }

    if(no[1]!=1 || no[1] != "1") {
        $('#'+div_id).val("");
    }
}
